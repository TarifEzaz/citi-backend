package dto

import "citi-backend/model"

// ReadReq stores order read request data
type ReadReq struct {
	UserID    string
	ContestID string
}

// ReadReq stores order read request data
type ReadResp struct {
	ContestID  string
	Name       string
	LandingURL string
	ImageURL   string
	Standings  string
}

// FromModel converts the model data to response data
func (r *ReadResp) FromModel(mc *model.Contest) {
	r.Name = mc.Name
	r.Standings = mc.Stadings
	r.ImageURL = mc.ImageURL
	r.LandingURL = mc.LandingURL
}
