package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"citi-backend/errors"

	validator "gopkg.in/go-playground/validator.v9"
)

// contest provides dto for contest request
type Create struct {
	ID         string `json:"contest_id"`
	Name       string `json:"name"`
	LandingURL string `json:"landing_url"`
	Standings  string `json:"standings"`
	ImageURL   string `json:"image_url"`
	IsDeleted  bool   `json:"is_deleted"`
}

// Validate validates contest request data
func (d *Create) Validate(validate *validator.Validate) error {
	if err := validate.Struct(d); err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{"invalid data for contest", false},
			},
		)
	}
	return nil
}

// FromReader reads contest request from request body
func (d *Create) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(d)
	if err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			Base: errors.Base{"invalid contest data", false},
		})
	}

	return nil
}
