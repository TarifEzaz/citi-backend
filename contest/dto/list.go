package dto

//ReadReq stores order read request data
type ListReq struct {
	UserID string
	Contest  string
}
