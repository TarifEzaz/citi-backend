package author

import (
	"citi-backend/author/dto"
	"citi-backend/errors"
	"citi-backend/model"
	storeauthor "citi-backend/store/author"
	storecontest "citi-backend/store/contest"
	storeproblem "citi-backend/store/problem"
	"fmt"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

// Reader provides an interface for reading contestes
type Lister interface {
	List(skip int64, limit int64) ([]dto.ListItem, error)
}

// contestReader implements Reader interface
type contestLister struct {
	authors  storeauthor.Authors
	contests storecontest.Contests
	problems storeproblem.Problems
}

func (list *contestLister) askStore(skip int64, limit int64) (
	author []*model.Author,
	err error,
) {
	author, err = list.authors.FindAll(skip, limit)
	return
}

func (list *contestLister) giveError() (err error) {
	err = &errors.Unknown{
		errors.Base{
			"Invalid request", false,
		},
	}
	return
}

func (list *contestLister) prepareResponse(
	authors []*model.Author,
) (
	resp []dto.ReadResp,
) {
	for _, author := range authors {
		var tmp dto.ReadResp
		tmp.FromModel(author)
		resp = append(resp, tmp)
	}
	//resp.FromModel(author)
	return
}

func (list *contestLister) List(skip int64, limit int64) ([]dto.ListItem, error) {
	//TO-DO: some validation on the input data is required
	authors, err := list.askStore(skip, limit)
	var resp []dto.ListItem

	if err != nil {
		logrus.Error("Could not find author error : ", err)
		return nil, list.giveError()
	}

	for _, author := range authors {
		//var contests []model.Contest
		//var problems []model.Problem
		var contests []dto.Contest
		var problems []dto.Problem

		for _, contest := range author.Contests {
			contest, err := list.contests.FindByID(contest)
			fmt.Println(err)
			var cur_contest dto.Contest
			cur_contest.FromModel(contest)
			contests = append(contests, cur_contest) // not sure about the asterisk
		}

		for _, problem := range author.Problems {
			problem, err := list.problems.FindByID(problem)
			fmt.Println(err)
			var cur_problem dto.Problem
			cur_problem.FromModel(problem)
			problems = append(problems, cur_problem)
		}

		var cur_author dto.ListItem
		cur_author.FromModel(author, contests, problems)
		resp = append(resp, cur_author)
	}

	return resp, nil
}

// NewReaderParams lists params for the NewReader
type NewListerParams struct {
	dig.In
	Author  storeauthor.Authors
	Problem storeproblem.Problems
	Contest storecontest.Contests
}

// NewReader provides Reader
func NewList(params NewListerParams) Lister {
	return &contestLister{
		authors:  params.Author,
		problems: params.Problem,
		contests: params.Contest,
	}
}
