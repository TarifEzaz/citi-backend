package author

import (
	"fmt"
	"time"

	"citi-backend/author/dto"
	"citi-backend/errors"
	"citi-backend/model"
	storeauthor "citi-backend/store/author"

	"github.com/sirupsen/logrus"
	validator "gopkg.in/go-playground/validator.v9"
)

// Updater provides an interface for updating contests
type Updater interface {
	Update(*dto.UpdateReq) (*dto.UpdateResponse, error)
}

// update updates author
type update struct {
	storecontest storeauthor.Authors
	validate     *validator.Validate
}

func (u *update) toModel(usercontest *dto.UpdateReq) (author *model.Author) {

	author = &model.Author{}

	author.UpdatedAt = time.Now().UTC()
	author.ID = usercontest.ID
	author.Name = usercontest.Name
	author.Contests = usercontest.Contests
	author.Problems = usercontest.Problems
	author.Intro = usercontest.Intro
	author.Tagline = usercontest.Tagline
	return
}

func (u *update) validateData(update *dto.UpdateReq) (err error) {
	err = update.Validate(u.validate)
	return
}

func (u *update) convertData(update *dto.UpdateReq) (
	modelcontest *model.Author,
) {
	modelcontest = u.toModel(update)
	return
}

func (u *update) askStore(modelcontest *model.Author) (
	id string,
	err error,
) {
	id, err = u.storecontest.Save(modelcontest)
	return
}

func (u *update) giveResponse(
	modelcontest *model.Author,
	id string,
) *dto.UpdateResponse {
	logrus.WithFields(logrus.Fields{
		//		"id": modelcontest.UserID,
	}).Debug("User updated author successfully")

	return &dto.UpdateResponse{
		Message:    "author updated",
		OK:         true,
		ID:         id,
		UpdateTime: modelcontest.UpdatedAt.String(),
	}
}

func (u *update) giveError() (err error) {
	errResp := errors.Unknown{
		Base: errors.Base{
			OK:      false,
			Message: "Invalid data",
		},
	}
	err = fmt.Errorf(
		"%s %w",
		err.Error(),
		&errResp,
	)
	return
}

// Update implements Update interface
func (u *update) Update(update *dto.UpdateReq) (
	*dto.UpdateResponse, error,
) {
	if err := u.validateData(update); err != nil {
		return nil, err
	}

	modelcontest := u.convertData(update)
	id, err := u.askStore(modelcontest)
	if err == nil {
		return u.giveResponse(modelcontest, id), nil
	}

	logrus.Error("Could not update author ", err)
	err = u.giveError()
	return nil, err
}

// NewUpdate returns new instance of NewUpdate
func NewUpdate(store storeauthor.Authors, validate *validator.Validate) Updater {
	return &update{
		store,
		validate,
	}
}
