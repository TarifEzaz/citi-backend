package dto

import "citi-backend/model"

// ReadReq stores order read request data
type ListReq struct {
	UserID     string
	Author     string
	ContestIDs string
}

type Contest struct {
	ID         string `json:"contest_id"`
	Name       string `json:"name"`
	Standings  string `json:"standings"`
	ImageURL   string `json:"image_url"`
	LandingURL string `json:"landing_url"`
}

func (r *Contest) FromModel(contest *model.Contest) {
	r.ID = contest.ID
	r.Name = contest.Name
	r.Standings = contest.Stadings
	r.LandingURL = contest.LandingURL
	r.ImageURL = contest.ImageURL
}

type Problem struct {
	ID       string   `json:"contest_id"`
	Name     string   `json:"name"`
	Category []string `json:"category"`
}

func (r *Problem) FromModel(problem *model.Problem) {
	r.ID = problem.ID
	r.Name = problem.Name
	r.Category = problem.Category
}

// ReadResp holds the response data for reading restaurant
type ListItem struct {
	ID       string    `json:"id"`
	Name     string    `json:"name"`
	Intro    string    `json:"intro"`
	Tagline  string    `json:"tagline"`
	Contests []Contest `json:"contests"`
	Problems []Problem `json:"problems"`
}

func (r *ListItem) FromModel(author *model.Author, contest []Contest, problems []Problem) {
	r.ID = author.ID
	r.Name = author.Name
	r.Intro = author.Intro
	r.Tagline = author.Tagline
	r.Contests = contest
	r.Problems = problems
}

type List struct {
	All []ListItem `json:"list"`
}

func (r *List) FromModel(author *model.Author) {
	// r.ID = author.ID
	// r.Name = author.Name
}
