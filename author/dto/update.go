package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"citi-backend/errors"

	validator "gopkg.in/go-playground/validator.v9"
)

// Update provides dto for author update
type UpdateReq struct {
	ID       string   `json:"contest_id"`
	Name     string   `json:"name"`
	Intro    string   `json:"intro"`
	Tagline  string   `json:"tagline"`
	Contests []string `json:"contests"`
	Problems []string `json:problems"`
}

// Validate validates author update data
func (u *UpdateReq) Validate(validate *validator.Validate) error {
	if err := validate.Struct(u); err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{"invalid author update data", false},
			},
		)
	}
	return nil
}

// FromReader decodes author update data from request
func (u *UpdateReq) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(u)
	if err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				Base: errors.Base{"invalid author update data", false},
			},
		)
	}

	return nil
}
