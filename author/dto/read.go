package dto

//ReadReq stores order read request data
type ReadReq struct {
	UserID  string
	AuthorID string
}
