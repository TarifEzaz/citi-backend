package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"citi-backend/errors"

	validator "gopkg.in/go-playground/validator.v9"
)

// author provides dto for author request
type Author struct {
	ID       string   `json:"author_id"`
	Name     string   `json:"name"`
	Intro    string   `json:"intro"`
	Tagline  string   `json:"tagline"`
	Contests []string `json:"contests"`
	Problems []string `json:"problems"`
}

// Validate validates author request data
func (d *Author) Validate(validate *validator.Validate) error {
	if err := validate.Struct(d); err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{"invalid data for author", false},
			},
		)
	}
	return nil
}

// FromReader reads author request from request body
func (d *Author) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(d)
	if err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			Base: errors.Base{"invalid author data", false},
		})
	}

	return nil
}
