package dto

import "citi-backend/model"

// ReadResp holds the response data for reading restaurant
type ReadResp struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Tagline string `json:"tagline"`
}

// FromModel converts the model data to response data
func (r *ReadResp) FromModel(author *model.Author) {
	r.ID = author.ID
	r.Name = author.Name
}
