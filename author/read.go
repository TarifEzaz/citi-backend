package author

import (
	"citi-backend/author/dto"
	"citi-backend/errors"
	"citi-backend/model"
	storeauthor "citi-backend/store/author"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

// Reader provides an interface for reading contestes
type Reader interface {
	Read(*dto.ReadReq) (*dto.ReadResp, error)
}

// contestReader implements Reader interface
type contestReader struct {
	contests storeauthor.Authors
}

func (read *contestReader) askStore(contestID string) (
	author *model.Author,
	err error,
) {
	author, err = read.contests.FindByID(contestID)
	return
}

func (read *contestReader) giveError() (err error) {
	err = &errors.Unknown{
		errors.Base{
			"Invalid request", false,
		},
	}
	return
}

func (read *contestReader) prepareResponse(
	author *model.Author,
) (
	resp dto.ReadResp,
) {
	resp.FromModel(author)
	return
}

func (read *contestReader) Read(contestReq *dto.ReadReq) (*dto.ReadResp, error) {
	//TO-DO: some validation on the input data is required
	author, err := read.askStore(contestReq.AuthorID)
	if err != nil {
		logrus.Error("Could not find author error : ", err)
		return nil, read.giveError()
	}

	var resp dto.ReadResp
	resp = read.prepareResponse(author)

	return &resp, nil
}

// NewReaderParams lists params for the NewReader
type NewReaderParams struct {
	dig.In
	Author storeauthor.Authors
}

// NewReader provides Reader
func NewReader(params NewReaderParams) Reader {
	return &contestReader{
		contests: params.Author,
	}
}
