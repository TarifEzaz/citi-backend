package author

import (
	"fmt"
	"time"

	"citi-backend/author/dto"
	"citi-backend/errors"
	"citi-backend/model"
	storeauthor "citi-backend/store/author"

	"github.com/sirupsen/logrus"
	validator "gopkg.in/go-playground/validator.v9"
)

// Deleter provides an interface for updating contests
type Deleter interface {
	Delete(*dto.Delete) (*dto.DeleteResponse, error)
}

// delete deletes author
type delete struct {
	storeauthor storeauthor.Authors
	validate    *validator.Validate
}

func (d *delete) toModel(usercontest *dto.Delete) (author *model.Author) {
	author = &model.Author{}

	author.UpdatedAt = time.Now().UTC()
	author.IsDeleted = true
	//	author.UserID = usercontest.UserID
	return
}

func (d *delete) validateData(delete *dto.Delete) (err error) {
	err = delete.Validate(d.validate)
	return
}

func (d *delete) convertData(delete *dto.Delete) (
	modelcontest *model.Author,
) {
	modelcontest = d.toModel(delete)
	return
}

func (d *delete) askStore(modelcontest *model.Author) (
	id string,
	err error,
) {
	id, err = d.storeauthor.Save(modelcontest)
	return
}

func (d *delete) giveResponse(
	modelNotice *model.Author,
	id string,
) *dto.DeleteResponse {
	logrus.WithFields(logrus.Fields{
		//		"id": modelNotice.UserID,
	}).Debug("User deleted author successfully")

	return &dto.DeleteResponse{
		Message: "author deleted",
		OK:      true,
		ID:      id,
		//		DeleteTime: modelNotice.DeletedAt.String(),
	}
}

func (d *delete) giveError() (err error) {
	errResp := errors.Unknown{
		Base: errors.Base{
			OK:      false,
			Message: "Invalid data",
		},
	}
	err = fmt.Errorf(
		"%s %w",
		err.Error(),
		&errResp,
	)
	return
}

// Delete implements Delete interface
func (d *delete) Delete(delete *dto.Delete) (
	*dto.DeleteResponse, error,
) {
	if err := d.validateData(delete); err != nil {
		return nil, err
	}

	modelcontest := d.convertData(delete)
	id, err := d.askStore(modelcontest)
	if err == nil {
		return d.giveResponse(modelcontest, id), nil
	}

	logrus.Error("Could not delete author ", err)
	err = d.giveError()
	return nil, err
}

// NewDelete returns new instance of NewDelete
func NewDelete(store storeauthor.Authors, validate *validator.Validate) Deleter {
	return &delete{
		store,
		validate,
	}
}
