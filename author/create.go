package author

import (
	"fmt"
	"time"

	"citi-backend/author/dto"
	"citi-backend/errors"
	"citi-backend/model"
	storeauthor "citi-backend/store/author"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
	validator "gopkg.in/go-playground/validator.v9"
)

// Creater provides create method for creating author
type Creater interface {
	Create(create *dto.Author) (*dto.CreateResponse, error)
}

// create creates author
type create struct {
	storeauthor storeauthor.Authors
	validate    *validator.Validate
}

func (c *create) toModel(usercontest *dto.Author) (
	author *model.Author,
) {
	author = &model.Author{}
	author.CreatedAt = time.Now().UTC()
	author.UpdatedAt = author.CreatedAt
	author.ID = usercontest.ID
	author.Name = usercontest.Name
	author.Intro = usercontest.Intro
	author.Problems = usercontest.Problems
	author.Contests = usercontest.Contests

	return
}

func (c *create) validateData(create *dto.Author) (
	err error,
) {
	err = create.Validate(c.validate)
	return
}

func (c *create) convertData(create *dto.Author) (
	modelcontest *model.Author,
) {
	modelcontest = c.toModel(create)
	return
}

func (c *create) askStore(model *model.Author) (
	id string,
	err error,
) {
	id, err = c.storeauthor.Save(model)
	return
}

func (c *create) giveResponse(modelcontest *model.Author, id string) (
	*dto.CreateResponse, error,
) {
	logrus.WithFields(logrus.Fields{
		//		"id": modelcontest.UserID,
	}).Debug("User created author successfully")

	return &dto.CreateResponse{
		Message: "author created",
		OK:      true,
		ID:      id,
	}, nil
}

func (c *create) giveError() (err error) {
	logrus.Error("Could not create author. Error: ", err)
	errResp := errors.Unknown{
		Base: errors.Base{
			OK:      false,
			Message: "Invalid data",
		},
	}

	err = fmt.Errorf("%s %w", err.Error(), &errResp)
	return
}

// Create implements Creater interface
func (c *create) Create(create *dto.Author) (
	*dto.CreateResponse, error,
) {
	err := c.validateData(create)
	if err != nil {
		return nil, err
	}

	modelcontest := c.convertData(create)
	id, err := c.askStore(modelcontest)
	if err == nil {
		return c.giveResponse(modelcontest, id)
	}

	err = c.giveError()
	return nil, err
}

// CreateParams give parameters for NewCreate
type CreateParams struct {
	dig.In
	Storeauthors storeauthor.Authors
	Validate     *validator.Validate
}

// NewCreate returns new instance of NewCreate
func NewCreate(params CreateParams) Creater {
	return &create{
		params.Storeauthors,
		params.Validate,
	}
}
