package model

import (
	"time"

	"citi-backend/model"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Contest holds db data type for deliveries
type Contest struct {
	ID         primitive.ObjectID `bson:"_id,omitempty"`
	Name       string             `bson:"name,omitempty"`
	Standings  string             `bson:"standings,omitempty"`
	LandingURL string             `bson:"landing_url"`
	ImageURL   string             `bson:"image_url,omitempty"`
	CreatedAt  time.Time          `bson:"created_at,omitempty"`
	UpdatedAt  time.Time          `bson:"updated_at,omitempty"`
	IsDeleted  bool               `bson:"is_deleted,omitempty"`
}

// FromModel converts model data to db data for deliveries
func (d *Contest) FromModel(modelContest *model.Contest) error {
	d.CreatedAt = modelContest.CreatedAt
	d.UpdatedAt = modelContest.UpdatedAt

	d.Name = modelContest.Name
	d.Standings = modelContest.Stadings
	d.LandingURL = modelContest.LandingURL
	d.ImageURL = modelContest.ImageURL

	var err error

	if modelContest.ID != "" {
		d.ID, err = primitive.ObjectIDFromHex(modelContest.ID)
	} else {
		d.ID = primitive.NewObjectID()
	}

	if err != nil {
		return err
	}

	return nil
}

// ModelDelivery converts bson to model
func (d *Contest) ModelContest() *model.Contest {
	Contest := model.Contest{}
	Contest.ID = d.ID.Hex()
	Contest.CreatedAt = d.CreatedAt
	Contest.UpdatedAt = d.UpdatedAt

	Contest.Name = d.Name
	Contest.Stadings = d.Standings
	Contest.ImageURL = d.ImageURL
	Contest.LandingURL = d.LandingURL

	return &Contest
}
