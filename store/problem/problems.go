package problem

import "citi-backend/model"

// Problems wraps delivery's functionality
type Problems interface {
	Save(*model.Problem) (id string, err error)
	FindByID(id string) (*model.Problem, error)
	FindByProblemID(id string, skip int64, limit int64) ([]*model.Problem, error)
	CountByProblemID(id string) (int64, error)
	FindByIDs(id ...string) ([]*model.Problem, error)
	Search(q string, skip, limit int64) ([]*model.Problem, error)
	FindByUser(id string, skip, limit int64) ([]*model.Problem, error)
	FindByDriver(id string) (*model.Problem, error)
}
