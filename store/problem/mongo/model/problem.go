package model

import (
	"citi-backend/model"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Problem struct {
	ID         primitive.ObjectID   `bson:"_id,omitempty"`
	Name       string               `bson:"name,omitempty"`
	CreatedAt  time.Time            `bson:"created_at,omitempty"`
	UpdatedAt  time.Time            `bson:"updated_at,omitempty"`
	Category   []string             `bson:"cateogyr,omitempty"`
	Authors    []primitive.ObjectID `bson:"authors,omitempty"`
	Difficulty float32              `bson:"difficulty, omitempty"`
	Contest    primitive.ObjectID   `bson:"contest,omitempty"`
}

// FromModel converts model data to db data for deliveries
func (d *Problem) FromModel(modelProblem *model.Problem) error {
	d.CreatedAt = modelProblem.CreatedAt
	d.UpdatedAt = modelProblem.UpdatedAt

	d.Name = modelProblem.Name
	d.Category = modelProblem.Category
	d.Difficulty = modelProblem.Difficulty

	var err error

	if modelProblem.ID != "" {
		d.ID, err = primitive.ObjectIDFromHex(modelProblem.ID)
	} else {
		d.ID = primitive.NewObjectID()
	}

	for _, val := range modelProblem.Authors {
		obj, err := primitive.ObjectIDFromHex(val)
		if err != nil {
			fmt.Println(err)
		} else {
			d.Authors = append(d.Authors, obj)
		}
	}

	if modelProblem.Contest != "" {
		d.Contest, err = primitive.ObjectIDFromHex(modelProblem.Contest)
	}

	if err != nil {
		return err
	}

	return nil
}

// ModelDelivery converts bson to model
func (d *Problem) ModelProblem() *model.Problem {
	Problem := model.Problem{}
	Problem.ID = d.ID.Hex()
	Problem.CreatedAt = d.CreatedAt
	Problem.UpdatedAt = d.UpdatedAt
	Problem.Contest = d.Contest.Hex()
	Problem.Name = d.Name
	Problem.Difficulty = d.Difficulty
	Problem.Category = d.Category

	for _, val := range d.Authors {
		Problem.Authors = append(Problem.Authors, val.Hex())
	}

	return &Problem
}
