package mongo

import (
	"context"
	"fmt"

	"citi-backend/model"
	storeproblem "citi-backend/store/problem"
	mongoModel "citi-backend/store/problem/mongo/model"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/dig"
)

// Problems handles problem related database queries
type Problems struct {
	c *mongo.Collection
}

func (d *Problems) convertData(modelProblem *model.Problem) (
	mongoProblem mongoModel.Problem,
	err error,
) {
	err = mongoProblem.FromModel(modelProblem)
	return
}

// Save saves Problems from model to database
func (d *Problems) Save(modelProblem *model.Problem) (string, error) {
	mongoProblem := mongoModel.Problem{}
	var err error
	mongoProblem, err = d.convertData(modelProblem)
	if err != nil {
		return "", fmt.Errorf("Could not convert model problem to mongo problem: %w", err)
	}

	if modelProblem.ID == "" {
		mongoProblem.ID = primitive.NewObjectID()
	}

	filter := bson.M{"_id": mongoProblem.ID}
	update := bson.M{"$set": mongoProblem}
	upsert := true

	_, err = d.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{
			Upsert: &upsert,
		},
	)

	return mongoProblem.ID.Hex(), err
}

// FindByID finds a problem by id
func (d *Problems) FindByID(id string) (*model.Problem, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"_id": objectID}
	result := d.c.FindOne(
		context.Background(),
		filter,
		&options.FindOneOptions{},
	)
	if err := result.Err(); err != nil {
		return nil, err
	}

	problem := mongoModel.Problem{}
	if err := result.Decode(&problem); err != nil {
		return nil, fmt.Errorf("Could not decode mongo model to model : %w", err)
	}

	return problem.ModelProblem(), nil
}

// FindByProblemID finds a problem by problem id
func (d *Problems) FindByProblemID(id string, skip int64, limit int64) ([]*model.Problem, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"problem_id": objectID}

	findOptions := options.Find()
	findOptions.SetSort(map[string]int{"updated_at": -1})
	findOptions.SetSkip(skip)
	findOptions.SetLimit(limit)

	cursor, err := d.c.Find(context.Background(), filter, findOptions)

	if err != nil {
		return nil, err
	}

	return d.cursorToDeliveries(cursor)
}

// CountByProblemID returns Problems from problem id
func (d *Problems) CountByProblemID(id string) (int64, error) {
	objectID, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return -1, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"status_id": objectID}
	cnt, err := d.c.CountDocuments(context.Background(), filter, &options.CountOptions{})

	if err != nil {
		return -1, err
	}

	return cnt, nil
}

// FindByIDs returns all the Problems from multiple problem ids
func (d *Problems) FindByIDs(ids ...string) ([]*model.Problem, error) {
	objectIDs := []primitive.ObjectID{}
	for _, id := range ids {
		objectID, err := primitive.ObjectIDFromHex(id)
		if err != nil {
			return nil, fmt.Errorf("Invalid id %s : %w", id, err)
		}

		objectIDs = append(objectIDs, objectID)
	}

	filter := bson.M{
		"_id": bson.M{
			"$in": objectIDs,
		},
	}

	cursor, err := d.c.Find(context.Background(), filter, nil)
	if err != nil {
		return nil, err
	}

	return d.cursorToDeliveries(cursor)
}

// Search search for Problems given the text, skip and limit
func (d *Problems) Search(text string, skip, limit int64) ([]*model.Problem, error) {
	filter := bson.M{"$text": bson.M{"$search": text}}
	cursor, err := d.c.Find(
		context.Background(),
		filter,
		&options.FindOptions{
			Skip:  &skip,
			Limit: &limit,
		},
	)
	if err != nil {
		return nil, err
	}

	return d.cursorToDeliveries(cursor)
}

// Search search for Problems given the text, skip and limit
func (d *Problems) FindByUser(id string, skip, limit int64) ([]*model.Problem, error) {
	filter := bson.M{"_id": id}
	cursor, err := d.c.Find(
		context.Background(),
		filter,
		&options.FindOptions{
			Skip:  &skip,
			Limit: &limit,
		},
	)
	if err != nil {
		return nil, err
	}

	return d.cursorToDeliveries(cursor)
}

// Search search for Problems given the text, skip and limit
func (d *Problems) FindByDriver(id string) (*model.Problem, error) {
	driverID, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"driver_id": driverID, "is_active": true, "state": "pending"}

	result := d.c.FindOne(
		context.Background(),
		filter,
		&options.FindOneOptions{},
	)

	if err := result.Err(); err != nil {
		return nil, err
	}

	problem := mongoModel.Problem{}
	if err := result.Decode(&problem); err != nil {
		return nil, fmt.Errorf("Could not decode mongo model to model : %w", err)
	}

	return problem.ModelProblem(), nil
}

// cursorToDeliveries decodes Problems one by one from the search result
func (d *Problems) cursorToDeliveries(cursor *mongo.Cursor) ([]*model.Problem, error) {
	defer cursor.Close(context.Background())
	modelDeliveries := []*model.Problem{}

	for cursor.Next(context.Background()) {
		problem := mongoModel.Problem{}
		if err := cursor.Decode(&problem); err != nil {
			return nil, fmt.Errorf("Could not decode data from mongo %w", err)
		}

		modelDeliveries = append(modelDeliveries, problem.ModelProblem())
	}

	return modelDeliveries, nil
}

// DeliveriesParams provides parameters for problem specific Collection
type DeliveriesParams struct {
	dig.In
	Collection *mongo.Collection `name:"problems"`
}

// Store provides store for Problems
func Store(params DeliveriesParams) storeproblem.Problems {
	return &Problems{params.Collection}
}
