package author

import "citi-backend/model"

// Authors wraps delivery's functionality
type Authors interface {
	Save(*model.Author) (id string, err error)
	FindByID(id string) (*model.Author, error)
	FindByAuthorID(id string, skip int64, limit int64) ([]*model.Author, error)
	CountByAuthorID(id string) (int64, error)
	FindByIDs(id ...string) ([]*model.Author, error)
	Search(q string, skip, limit int64) ([]*model.Author, error)
	FindAll(skip, limit int64) ([]*model.Author, error)
	FindByUser(id string, skip, limit int64) ([]*model.Author, error)
}
