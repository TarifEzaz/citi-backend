package model

import (
	"fmt"
	"time"

	"citi-backend/model"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Author holds db data type for deliveries
type Author struct {
	ID        primitive.ObjectID   `bson:"_id,omitempty"`
	Name      string               `bson:"name,omitempty"`
	CreatedAt time.Time            `bson:"created_at,omitempty"`
	UpdatedAt time.Time            `bson:"updated_at,omitempty"`
	Intro     string               `bson:"intro,omitempty"`
	Tagline   string               `bson:"tagline,omitempty"`
	Problems  []primitive.ObjectID `bson:"problems,omitempty"`
	Contests  []primitive.ObjectID `bson:"contests,omitempty"`
	IsDeleted bool                 `bson:"is_deleted,omitempty"`
}

// FromModel converts model data to db data for deliveries
func (d *Author) FromModel(modelAuthor *model.Author) error {
	d.CreatedAt = modelAuthor.CreatedAt
	d.UpdatedAt = modelAuthor.UpdatedAt
	d.Name = modelAuthor.Name
	d.IsDeleted = modelAuthor.IsDeleted
	d.Intro = modelAuthor.Intro
	d.Tagline = modelAuthor.Tagline

	for _, val := range modelAuthor.Problems {
		obj, err := primitive.ObjectIDFromHex(val)
		if err != nil {
			fmt.Println(err)
		} else {
			d.Problems = append(d.Problems, obj)
		}
	}

	for _, val := range modelAuthor.Contests {
		obj, err := primitive.ObjectIDFromHex(val)
		if err != nil {
			fmt.Println(err)
		} else {
			d.Contests = append(d.Contests, obj)
		}
	}

	return nil
}

// ModelDelivery converts bson to model
func (d *Author) ModelAuthor() *model.Author {
	Author := model.Author{}
	Author.ID = d.ID.Hex()
	Author.CreatedAt = d.CreatedAt
	Author.UpdatedAt = d.UpdatedAt

	Author.Name = d.Name
	Author.IsDeleted = d.IsDeleted
	Author.Intro = d.Intro
	Author.Tagline = d.Tagline

	for _, val := range d.Problems {
		Author.Problems = append(Author.Problems, val.Hex())
	}

	for _, val := range d.Contests {
		Author.Contests = append(Author.Contests, val.Hex())
	}

	return &Author
}
