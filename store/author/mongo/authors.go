package mongo

import (
	"context"
	"fmt"

	"citi-backend/model"
	storeauthor "citi-backend/store/author"
	mongoModel "citi-backend/store/author/mongo/model"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/dig"
)

// Authors handles author related database queries
type Authors struct {
	c *mongo.Collection
}

func (d *Authors) convertData(modelAuthor *model.Author) (
	mongoAuthor mongoModel.Author,
	err error,
) {
	err = mongoAuthor.FromModel(modelAuthor)
	return
}

// Save saves Authors from model to database
func (d *Authors) Save(modelAuthor *model.Author) (string, error) {
	mongoAuthor := mongoModel.Author{}
	var err error
	mongoAuthor, err = d.convertData(modelAuthor)
	if err != nil {
		return "", fmt.Errorf("Could not convert model author to mongo author: %w", err)
	}

	if modelAuthor.ID == "" {
		mongoAuthor.ID = primitive.NewObjectID()
	}

	filter := bson.M{"_id": mongoAuthor.ID}
	update := bson.M{"$set": mongoAuthor}
	upsert := true

	_, err = d.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{
			Upsert: &upsert,
		},
	)

	return mongoAuthor.ID.Hex(), err
}

// FindByID finds a author by id
func (d *Authors) FindByID(id string) (*model.Author, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"_id": objectID}
	result := d.c.FindOne(
		context.Background(),
		filter,
		&options.FindOneOptions{},
	)
	if err := result.Err(); err != nil {
		return nil, err
	}

	author := mongoModel.Author{}
	if err := result.Decode(&author); err != nil {
		return nil, fmt.Errorf("Could not decode mongo model to model : %w", err)
	}

	return author.ModelAuthor(), nil
}

// FindByAuthorID finds a author by author id
func (d *Authors) FindByAuthorID(id string, skip int64, limit int64) ([]*model.Author, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"author_id": objectID}

	findOptions := options.Find()
	findOptions.SetSort(map[string]int{"updated_at": -1})
	findOptions.SetSkip(skip)
	findOptions.SetLimit(limit)

	cursor, err := d.c.Find(context.Background(), filter, findOptions)

	if err != nil {
		return nil, err
	}

	return d.cursorToDeliveries(cursor)
}

// CountByAuthorID returns Authors from author id
func (d *Authors) CountByAuthorID(id string) (int64, error) {
	objectID, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return -1, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"status_id": objectID}
	cnt, err := d.c.CountDocuments(context.Background(), filter, &options.CountOptions{})

	if err != nil {
		return -1, err
	}

	return cnt, nil
}

// FindByIDs returns all the Authors from multiple author ids
func (d *Authors) FindByIDs(ids ...string) ([]*model.Author, error) {
	objectIDs := []primitive.ObjectID{}
	for _, id := range ids {
		objectID, err := primitive.ObjectIDFromHex(id)
		if err != nil {
			return nil, fmt.Errorf("Invalid id %s : %w", id, err)
		}

		objectIDs = append(objectIDs, objectID)
	}

	filter := bson.M{
		"_id": bson.M{
			"$in": objectIDs,
		},
	}

	cursor, err := d.c.Find(context.Background(), filter, nil)
	if err != nil {
		return nil, err
	}

	return d.cursorToDeliveries(cursor)
}

// Search search for Authors given the text, skip and limit
func (d *Authors) Search(text string, skip, limit int64) ([]*model.Author, error) {
	filter := bson.M{"$text": bson.M{"$search": text}}
	cursor, err := d.c.Find(
		context.Background(),
		filter,
		&options.FindOptions{
			Skip:  &skip,
			Limit: &limit,
		},
	)
	if err != nil {
		return nil, err
	}

	return d.cursorToDeliveries(cursor)
}

// Search search for Authors given the text, skip and limit
func (d *Authors) FindAll(skip, limit int64) ([]*model.Author, error) {
	filter := bson.M{}
	cursor, err := d.c.Find(
		context.Background(),
		filter,
		&options.FindOptions{
			Skip:  &skip,
			Limit: &limit,
		},
	)
	if err != nil {
		return nil, err
	}

	return d.cursorToDeliveries(cursor)
}

// Search search for Authors given the text, skip and limit
func (d *Authors) FindByUser(id string, skip, limit int64) ([]*model.Author, error) {
	filter := bson.M{"_id": id}
	cursor, err := d.c.Find(
		context.Background(),
		filter,
		&options.FindOptions{
			Skip:  &skip,
			Limit: &limit,
		},
	)
	if err != nil {
		return nil, err
	}

	return d.cursorToDeliveries(cursor)
}

// Search search for Authors given the text, skip and limit
func (d *Authors) FindByDriver(id string) (*model.Author, error) {
	driverID, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"driver_id": driverID, "is_active": true, "state": "pending"}

	result := d.c.FindOne(
		context.Background(),
		filter,
		&options.FindOneOptions{},
	)

	if err := result.Err(); err != nil {
		return nil, err
	}

	author := mongoModel.Author{}
	if err := result.Decode(&author); err != nil {
		return nil, fmt.Errorf("Could not decode mongo model to model : %w", err)
	}

	return author.ModelAuthor(), nil
}

// cursorToDeliveries decodes Authors one by one from the search result
func (d *Authors) cursorToDeliveries(cursor *mongo.Cursor) ([]*model.Author, error) {
	defer cursor.Close(context.Background())
	modelDeliveries := []*model.Author{}

	for cursor.Next(context.Background()) {
		author := mongoModel.Author{}
		if err := cursor.Decode(&author); err != nil {
			return nil, fmt.Errorf("Could not decode data from mongo %w", err)
		}

		modelDeliveries = append(modelDeliveries, author.ModelAuthor())
	}

	return modelDeliveries, nil
}

// DeliveriesParams provides parameters for author specific Collection
type DeliveriesParams struct {
	dig.In
	Collection *mongo.Collection `name:"authors"`
}

// Store provides store for Authors
func Store(params DeliveriesParams) storeauthor.Authors {
	return &Authors{params.Collection}
}
