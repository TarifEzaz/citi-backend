package problem

import (
	"fmt"
	"time"

	"citi-backend/errors"
	"citi-backend/model"
	"citi-backend/problem/dto"
	storeproblem "citi-backend/store/problem"

	"github.com/sirupsen/logrus"
	validator "gopkg.in/go-playground/validator.v9"
)

// Deleter provides an interface for updating contests
type Deleter interface {
	Delete(*dto.Delete) (*dto.DeleteResponse, error)
}

// delete deletes problem
type delete struct {
	storeproblem storeproblem.Problems
	validate     *validator.Validate
}

func (d *delete) toModel(usercontest *dto.Delete) (problem *model.Problem) {
	problem = &model.Problem{}

	problem.UpdatedAt = time.Now().UTC()
	return
}

func (d *delete) validateData(delete *dto.Delete) (err error) {
	err = delete.Validate(d.validate)
	return
}

func (d *delete) convertData(delete *dto.Delete) (
	modelcontest *model.Problem,
) {
	modelcontest = d.toModel(delete)
	return
}

func (d *delete) askStore(modelcontest *model.Problem) (
	id string,
	err error,
) {
	id, err = d.storeproblem.Save(modelcontest)
	return
}

func (d *delete) giveResponse(
	modelNotice *model.Problem,
	id string,
) *dto.DeleteResponse {
	logrus.WithFields(logrus.Fields{
		//		"id": modelNotice.UserID,
	}).Debug("User deleted problem successfully")

	return &dto.DeleteResponse{
		Message: "problem deleted",
		OK:      true,
		ID:      id,
		//		DeleteTime: modelNotice.DeletedAt.String(),
	}
}

func (d *delete) giveError() (err error) {
	errResp := errors.Unknown{
		Base: errors.Base{
			OK:      false,
			Message: "Invalid data",
		},
	}
	err = fmt.Errorf(
		"%s %w",
		err.Error(),
		&errResp,
	)
	return
}

// Delete implements Delete interface
func (d *delete) Delete(delete *dto.Delete) (
	*dto.DeleteResponse, error,
) {
	if err := d.validateData(delete); err != nil {
		return nil, err
	}

	modelcontest := d.convertData(delete)
	id, err := d.askStore(modelcontest)
	if err == nil {
		return d.giveResponse(modelcontest, id), nil
	}

	logrus.Error("Could not delete problem ", err)
	err = d.giveError()
	return nil, err
}

// NewDelete returns new instance of NewDelete
func NewDelete(store storeproblem.Problems, validate *validator.Validate) Deleter {
	return &delete{
		store,
		validate,
	}
}
