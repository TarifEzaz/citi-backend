package problem

import (
	"citi-backend/errors"
	"citi-backend/problem/dto"

	"citi-backend/model"
	storeproblem "citi-backend/store/problem"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

// Reader provides an interface for reading contestes
type Reader interface {
	Read(*dto.ReadReq) (*dto.ReadResp, error)
}

// contestReader implements Reader interface
type contestReader struct {
	contests storeproblem.Problems
}

func (read *contestReader) askStore(contestID string) (
	problem *model.Problem,
	err error,
) {
	problem, err = read.contests.FindByID(contestID)
	return
}

func (read *contestReader) giveError() (err error) {
	err = &errors.Unknown{
		errors.Base{
			"Invalid request", false,
		},
	}
	return
}

func (read *contestReader) prepareResponse(
	problem *model.Problem,
) (
	resp dto.ReadResp,
) {
	resp.FromModel(problem)
	return
}

func (read *contestReader) Read(contestReq *dto.ReadReq) (*dto.ReadResp, error) {
	//TO-DO: some validation on the input data is required
	problem, err := read.askStore(contestReq.ProblemID)
	if err != nil {
		logrus.Error("Could not find problem error : ", err)
		return nil, read.giveError()
	}

	var resp dto.ReadResp
	resp = read.prepareResponse(problem)

	return &resp, nil
}

// NewReaderParams lists params for the NewReader
type NewReaderParams struct {
	dig.In
	Problem storeproblem.Problems
}

// NewReader provides Reader
func NewReader(params NewReaderParams) Reader {
	return &contestReader{
		contests: params.Problem,
	}
}
