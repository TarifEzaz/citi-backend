package problem

import (
	"citi-backend/errors"
	"citi-backend/model"
	"citi-backend/problem/dto"
	storeproblem "citi-backend/store/problem"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

// Reader provides an interface for reading contestes
type Lister interface {
	List(req *dto.ListReq, skip int64, limit int64) ([]dto.ReadResp, error)
}

// contestReader implements Reader interface
type problemLister struct {
	problems storeproblem.Problems
}

func (list *problemLister) askStore(state string, skip int64, limit int64) (
	contest []*model.Problem,
	err error,
) {
	contest, err = list.problems.FindByProblemID(state, skip, limit)
	return
}

func (list *problemLister) giveError() (err error) {
	err = &errors.Unknown{
		errors.Base{
			"Invalid request", false,
		},
	}
	return
}

func (list *problemLister) prepareResponse(
	contests []*model.Problem,
) (
	resp []dto.ReadResp,
) {
	for _, contest := range contests {
		var tmp dto.ReadResp
		tmp.FromModel(contest)
		resp = append(resp, tmp)
	}
	//resp.FromModel(contest)
	return
}

func (read *problemLister) List(contestReq *dto.ListReq, skip int64, limit int64) ([]dto.ReadResp, error) {
	//TO-DO: some validation on the input data is required
	contests, err := read.askStore(contestReq.UserID, skip, limit)
	if err != nil {
		logrus.Error("Could not find contest error : ", err)
		return nil, read.giveError()
	}

	var resp []dto.ReadResp
	resp = read.prepareResponse(contests)

	return resp, nil
}

// NewReaderParams lists params for the NewReader
type NewListerParams struct {
	dig.In
	problem storeproblem.Problems
}

// NewReader provides Reader
func NewList(params NewListerParams) Lister {
	return &problemLister{
		problems: params.problem,
	}
}
