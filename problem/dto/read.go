package dto

import "citi-backend/model"

// ReadReq stores order read request data
type ReadReq struct {
	UserID    string
	ProblemID string
}

// ReadReq stores order read request data
type ReadResp struct {
	ProblemID  string
	Name       string
	Difficulty float32
	Category   []string
	Authors    []string
	Contest    string
}

// FromModel converts the model data to response data
func (r *ReadResp) FromModel(mp *model.Problem) {
	r.Name = mp.Name
	r.Difficulty = mp.Difficulty
	r.Category = mp.Category
	r.Authors = mp.Authors
	r.Contest = mp.Contest
}
