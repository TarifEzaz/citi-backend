package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"citi-backend/errors"

	validator "gopkg.in/go-playground/validator.v9"
)

// problem provides dto for problem request
type Problem struct {
	ID         string   `json:"contest_id"`
	IsActive   bool     `json:"is_active"`
	Name       string   `json:"name"`
	Category   []string `json:"category"`
	Difficulty float32  `json:"difficulty"`
	Authors    []string `json:"authors"`
	URL        string   `json:"problem_url"`
	Contest    string   `json:"contest"`
}

// Validate validates problem request data
func (d *Problem) Validate(validate *validator.Validate) error {
	if err := validate.Struct(d); err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{"invalid data for problem", false},
			},
		)
	}
	return nil
}

// FromReader reads problem request from request body
func (d *Problem) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(d)
	if err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			Base: errors.Base{"invalid problem data", false},
		})
	}

	return nil
}
