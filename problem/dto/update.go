package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"citi-backend/errors"

	validator "gopkg.in/go-playground/validator.v9"
)

// Update provides dto for problem update
type Update struct {
	ID         string   `json:"problem_id"`
	Name       string   `json:"name"`
	Category   []string `json:"category"`
	Difficulty float32  `json:"difficulty"`
	Authors    []string `json:"authors"`
	URL        string   `json:"problem_url"`
}

// Validate validates problem update data
func (u *Update) Validate(validate *validator.Validate) error {
	if err := validate.Struct(u); err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{"invalid problem update data", false},
			},
		)
	}
	return nil
}

// FromReader decodes problem update data from request
func (u *Update) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(u)
	if err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				Base: errors.Base{"invalid problem update data", false},
			},
		)
	}

	return nil
}
