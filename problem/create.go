package problem

import (
	"fmt"
	"time"

	"citi-backend/errors"
	"citi-backend/model"
	"citi-backend/problem/dto"

	storeproblem "citi-backend/store/problem"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
	validator "gopkg.in/go-playground/validator.v9"
)

// Creater provides create method for creating problem
type Creater interface {
	Create(create *dto.Problem) (*dto.CreateResponse, error)
}

// create creates problem
type create struct {
	storeproblem storeproblem.Problems
	validate     *validator.Validate
}

func (c *create) toModel(userprob *dto.Problem) (
	problem *model.Problem,
) {
	problem = &model.Problem{}
	problem.CreatedAt = time.Now().UTC()
	problem.UpdatedAt = problem.CreatedAt
	problem.ID = userprob.ID
	problem.Name = userprob.Name
	problem.Category = userprob.Category
	problem.Difficulty = userprob.Difficulty
	problem.Authors = userprob.Authors
	problem.Contest = userprob.Contest

	return
}

func (c *create) validateData(create *dto.Problem) (
	err error,
) {
	err = create.Validate(c.validate)
	return
}

func (c *create) convertData(create *dto.Problem) (
	modelcontest *model.Problem,
) {
	modelcontest = c.toModel(create)
	return
}

func (c *create) askStore(model *model.Problem) (
	id string,
	err error,
) {
	id, err = c.storeproblem.Save(model)
	return
}

func (c *create) giveResponse(modelcontest *model.Problem, id string) (
	*dto.CreateResponse, error,
) {
	logrus.WithFields(logrus.Fields{
		//		"id": modelcontest.UserID,
	}).Debug("User created problem successfully")

	return &dto.CreateResponse{
		Message: "problem created",
		OK:      true,
		//		contestTime: modelcontest.CreatedAt.String(),
		ID: id,
	}, nil
}

func (c *create) giveError() (err error) {
	logrus.Error("Could not create problem. Error: ", err)
	errResp := errors.Unknown{
		Base: errors.Base{
			OK:      false,
			Message: "Invalid data",
		},
	}

	err = fmt.Errorf("%s %w", err.Error(), &errResp)
	return
}

// Create implements Creater interface
func (c *create) Create(create *dto.Problem) (
	*dto.CreateResponse, error,
) {
	err := c.validateData(create)
	if err != nil {
		return nil, err
	}

	modelcontest := c.convertData(create)
	id, err := c.askStore(modelcontest)
	if err == nil {
		return c.giveResponse(modelcontest, id)
	}

	err = c.giveError()
	return nil, err
}

// CreateParams give parameters for NewCreate
type CreateParams struct {
	dig.In
	Storecontests storeproblem.Problems
	Validate      *validator.Validate
}

// NewCreate returns new instance of NewCreate
func NewCreate(params CreateParams) Creater {
	return &create{
		params.Storecontests,
		params.Validate,
	}
}
