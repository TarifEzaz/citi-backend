package server

import (
	"citi-backend/container"
	author "citi-backend/store/author/mongo"
	contest "citi-backend/store/contest/mongo"
	problem "citi-backend/store/problem/mongo"
	user "citi-backend/store/user/mongo"
)

// Store provides constructors for mongo db implementations
func Store(c container.Container) {
	c.Register(user.Store)
	c.Register(author.Store)
	c.Register(problem.Store)
	c.Register(contest.Store)
}
