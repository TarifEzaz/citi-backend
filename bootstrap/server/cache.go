package server

import (
	"citi-backend/cache/redis"
	"citi-backend/container"
)

func Cache(c container.Container) {
	c.Register(redis.NewSession)
	//	c.Register(redis.NewConnectionStatus)
}
