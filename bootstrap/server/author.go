package server

import (
	"citi-backend/author"
	"citi-backend/container"
)

// User registers user related providers
func Author(c container.Container) {
	c.Register(author.NewCreate)
	c.Register(author.NewDelete)
	c.Register(author.NewReader)
	c.Register(author.NewUpdate)
	c.Register(author.NewList)
}
