package server

import (
	"citi-backend/api"
	"citi-backend/container"
)

// Handler registers provider that returns root handler
func Handler(c container.Container) {
	c.Register(api.Handler)
}
