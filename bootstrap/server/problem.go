package server

import (
	"citi-backend/container"
	"citi-backend/problem"
)

// User registers user related providers
func Problem(c container.Container) {
	c.Register(problem.NewCreate)
	c.Register(problem.NewDelete)
	c.Register(problem.NewReader)
	c.Register(problem.NewUpdate)
}
