package server

import (
	"citi-backend/container"
	"citi-backend/token"
)

// Token registers token related providers
func Token(c container.Container) {
	c.Register(token.NewRegisterStore)
}
