package server

import (
	"citi-backend/container"
	"citi-backend/contest"
)

// User registers user related providers
func Contest(c container.Container) {
	c.Register(contest.NewCreate)
	c.Register(contest.NewDelete)
	c.Register(contest.NewReader)
	c.Register(contest.NewUpdate)
}
