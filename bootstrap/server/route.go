package server

import (
	"citi-backend/api/author"
	"citi-backend/api/contest"
	"citi-backend/api/problem"
	"citi-backend/api/routeutils"
	"citi-backend/api/user"

	"citi-backend/container"
)

// Route registers all the route providers to container
func Route(c container.Container) {
	c.RegisterGroup(routeutils.NewOptionRoute, "route")

	c.RegisterGroup(user.RegistrationRoute, "route")
	c.RegisterGroup(user.LoginRoute, "route")
	c.RegisterGroup(user.SearchRoute, "route")
	c.RegisterGroup(user.ListSuspendedRoute, "route")
	c.RegisterGroup(user.ListRoute, "route")

	c.RegisterGroup(author.CreateRoute, "route")
	c.RegisterGroup(author.ReadRoute, "route")
	c.RegisterGroup(author.DeleteRoute, "route")
	c.RegisterGroup(author.UpdateRoute, "route")
	c.RegisterGroup(author.ListRoute, "route")

	c.RegisterGroup(problem.CreateRoute, "route")
	c.RegisterGroup(problem.ReadRoute, "route")
	c.RegisterGroup(problem.DeleteRoute, "route")
	c.RegisterGroup(problem.UpdateRoute, "route")

	c.RegisterGroup(contest.CreateRoute, "route")
	c.RegisterGroup(contest.ReadRoute, "route")
	c.RegisterGroup(contest.DeleteRoute, "route")
	c.RegisterGroup(contest.UpdateRoute, "route")

}
