package contest

import (
	"io"
	"net/http"

	"citi-backend/api/middleware"
	"citi-backend/api/routeutils"
	"citi-backend/apipattern"
	"citi-backend/contest"
	"citi-backend/contest/dto"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

// deleteHandler holds contest item update handler
type deleteHandler struct {
	delete contest.Deleter
}

func (dh *deleteHandler) decodeBody(
	body io.ReadCloser,
) (
	contest dto.Delete,
	err error,
) {
	err = contest.FromReader(body)
	return
}

func (dh *deleteHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (dh *deleteHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return
}

func (dh *deleteHandler) askController(update *dto.Delete) (
	resp *dto.DeleteResponse,
	err error,
) {
	resp, err = dh.delete.Delete(update)
	return
}

func (dh *deleteHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.DeleteResponse,
) {
	routeutils.ServeResponse(
		w,
		http.StatusOK,
		resp,
	)
}

// ServeHTTP implements http.Handler interface
func (dh *deleteHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	contest := dto.Delete{}
	contest, err := dh.decodeBody(r.Body)

	if err != nil {
		message := "Unable to decode contest item delete error: "
		dh.handleError(w, err, message)
		return
	}

	contest.UserID = dh.decodeContext(r)

	data, err := dh.askController(&contest)

	if err != nil {
		message := "Unable to delete contest item error: "
		dh.handleError(w, err, message)
		return
	}

	dh.responseSuccess(w, data)
}

// DeleteParams provide parameters for contest delete handler
type DeleteParams struct {
	dig.In
	Delete     contest.Deleter
	Middleware *middleware.Auth
}

// DeleteRoute provides a route that deletes contest
func DeleteRoute(params DeleteParams) *routeutils.Route {
	handler := deleteHandler{params.Delete}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.ContestDelete,
		Handler: params.Middleware.Middleware(&handler),
	}
}
