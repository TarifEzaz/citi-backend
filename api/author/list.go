package author

import (
	"fmt"
	"net/http"
	"strconv"

	"citi-backend/api/middleware"
	"citi-backend/api/routeutils"
	"citi-backend/apipattern"
	"citi-backend/author"
	"citi-backend/author/dto"
	"citi-backend/errors"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
)

// searchHandler holds the handler that searches for restaurant
type listHandler struct {
	searcher author.Lister
}

// querySkip skips number of authors for a request
func (list *listHandler) querySkip(
	r *http.Request,
) (skip int, err error) {
	skip, err = strconv.Atoi(chi.URLParam(r, "skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid skip", false,
				},
			},
		)
		return
	}
	return
}

// queryLimit limits number of authors per query
func (list *listHandler) queryLimit(r *http.Request) (
	limit int, err error,
) {
	limit, err = strconv.Atoi(chi.URLParam(r, "limit"))

	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid limit", false,
				},
			},
		)
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too big", false,
			},
		}
		return
	}

	return
}

func (list *listHandler) askController(
	skip int64,
	limit int64,
) (
	resp []dto.ListItem,
	err error,
) {
	resp, err = list.searcher.List(int64(skip), int64(limit))
	return
}

func (list *listHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (list *listHandler) responseSuccess(
	w http.ResponseWriter,
	resp []dto.ListItem,
) {
	// Serve a response to the client
	routeutils.ServeResponse(
		w,
		http.StatusOK,
		resp,
	)
}

func (list *listHandler) handleRead(
	w http.ResponseWriter,
	r *http.Request,
) {

	skip, err := list.querySkip(r)

	if err != nil {
		//message := "Unable to decode error: "
		list.handleError(w, err)
		return
	}

	limit, err := list.queryLimit(r)

	// Read request from database using request id and author id
	resp, err := list.askController(int64(skip), int64(limit))

	if err != nil {
		list.handleError(w, err)
		return
	}

	list.responseSuccess(w, resp)
}

// ServeHTTP implements http.Handler
func (list *listHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	list.handleRead(w, r)
}

// SearchRoute provides a route that searches for restaurants
func ListRoute(
	searcher author.Lister,
	middleware *middleware.Auth,
) *routeutils.Route {
	handler := listHandler{searcher}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.AuthorList,
		Handler: middleware.Middleware(&handler),
	}
}
