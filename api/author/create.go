package author

import (
	"io"
	"net/http"

	"citi-backend/api/middleware"
	"citi-backend/api/routeutils"
	"citi-backend/apipattern"
	"citi-backend/author"
	"citi-backend/author/dto"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

// createHandler holds handler for creating author items
type createHandler struct {
	create author.Creater
}

func (ch *createHandler) decodeBody(
	body io.ReadCloser,
) (
	author dto.Author,
	err error,
) {
	err = author.FromReader(body)
	return
}

func (ch *createHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (ch *createHandler) askController(
	author *dto.Author,
) (
	data *dto.CreateResponse,
	err error,
) {
	data, err = ch.create.Create(author)
	return
}

func (ch *createHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return
}

func (ch *createHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.CreateResponse,
) {
	routeutils.ServeResponse(
		w,
		http.StatusOK,
		resp,
	)
}

// ServeHTTP implements http.Handler interface
func (ch *createHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	authorDat, err := ch.decodeBody(r.Body)

	if err != nil {
		message := "Unable to decode error: "
		ch.handleError(w, err, message)
		return
	}

	//	authorDat.UserID = ch.decodeContext(r)
	data, err := ch.askController(&authorDat)

	if err != nil {
		message := "Unable to create author error: "
		ch.handleError(w, err, message)
		return
	}

	ch.responseSuccess(w, data)
}

// CreateParams provide parameters for CreateRoute
type CreateParams struct {
	dig.In
	Create     author.Creater
	Middleware *middleware.Auth
}

// CreateRoute provides a route that lets to take authors
func CreateRoute(params CreateParams) *routeutils.Route {
	handler := createHandler{params.Create}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.AuthorCreate,
		Handler: params.Middleware.Middleware(&handler),
	}
}
