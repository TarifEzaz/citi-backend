package author

import (
	"io"
	"net/http"

	"citi-backend/api/middleware"
	"citi-backend/api/routeutils"
	apipattern "citi-backend/apipattern"
	"citi-backend/author"
	"citi-backend/author/dto"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

// deleteHandler holds author item update handler
type deleteHandler struct {
	delete author.Deleter
}

func (dh *deleteHandler) decodeBody(
	body io.ReadCloser,
) (
	author dto.Delete,
	err error,
) {
	err = author.FromReader(body)
	return
}

func (dh *deleteHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (dh *deleteHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return
}

func (dh *deleteHandler) askController(update *dto.Delete) (
	resp *dto.DeleteResponse,
	err error,
) {
	resp, err = dh.delete.Delete(update)
	return
}

func (dh *deleteHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.DeleteResponse,
) {
	routeutils.ServeResponse(
		w,
		http.StatusOK,
		resp,
	)
}

// ServeHTTP implements http.Handler interface
func (dh *deleteHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	author := dto.Delete{}
	author, err := dh.decodeBody(r.Body)

	if err != nil {
		message := "Unable to decode author item delete error: "
		dh.handleError(w, err, message)
		return
	}

	author.UserID = dh.decodeContext(r)

	data, err := dh.askController(&author)

	if err != nil {
		message := "Unable to delete author item error: "
		dh.handleError(w, err, message)
		return
	}

	dh.responseSuccess(w, data)
}

// DeleteParams provide parameters for author delete handler
type DeleteParams struct {
	dig.In
	Delete     author.Deleter
	Middleware *middleware.Auth
}

// DeleteRoute provides a route that deletes author
func DeleteRoute(params DeleteParams) *routeutils.Route {
	handler := deleteHandler{params.Delete}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.AuthorDelete,
		Handler: params.Middleware.Middleware(&handler),
	}
}
