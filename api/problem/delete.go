package problem

import (
	"io"
	"net/http"

	"citi-backend/api/middleware"
	"citi-backend/api/routeutils"
	"citi-backend/apipattern"
	"citi-backend/problem"
	"citi-backend/problem/dto"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

// deleteHandler holds problem item update handler
type deleteHandler struct {
	delete problem.Deleter
}

func (dh *deleteHandler) decodeBody(
	body io.ReadCloser,
) (
	problem dto.Delete,
	err error,
) {
	err = problem.FromReader(body)
	return
}

func (dh *deleteHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (dh *deleteHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return
}

func (dh *deleteHandler) askController(update *dto.Delete) (
	resp *dto.DeleteResponse,
	err error,
) {
	resp, err = dh.delete.Delete(update)
	return
}

func (dh *deleteHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.DeleteResponse,
) {
	routeutils.ServeResponse(
		w,
		http.StatusOK,
		resp,
	)
}

// ServeHTTP implements http.Handler interface
func (dh *deleteHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	problem := dto.Delete{}
	problem, err := dh.decodeBody(r.Body)

	if err != nil {
		message := "Unable to decode problem item delete error: "
		dh.handleError(w, err, message)
		return
	}

	problem.UserID = dh.decodeContext(r)

	data, err := dh.askController(&problem)

	if err != nil {
		message := "Unable to delete problem item error: "
		dh.handleError(w, err, message)
		return
	}

	dh.responseSuccess(w, data)
}

// DeleteParams provide parameters for problem delete handler
type DeleteParams struct {
	dig.In
	Delete     problem.Deleter
	Middleware *middleware.Auth
}

// DeleteRoute provides a route that deletes problem
func DeleteRoute(params DeleteParams) *routeutils.Route {
	handler := deleteHandler{params.Delete}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.ProblemDelete,
		Handler: params.Middleware.Middleware(&handler),
	}
}
