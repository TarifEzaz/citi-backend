package problem

import (
	"io"
	"net/http"

	"citi-backend/api/middleware"
	"citi-backend/api/routeutils"
	"citi-backend/apipattern"
	"citi-backend/problem"
	"citi-backend/problem/dto"

	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

// createHandler holds handler for creating problem items
type createHandler struct {
	create problem.Creater
}

func (ch *createHandler) decodeBody(
	body io.ReadCloser,
) (
	problem dto.Problem,
	err error,
) {
	err = problem.FromReader(body)
	return
}

func (ch *createHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (ch *createHandler) askController(
	problem *dto.Problem,
) (
	data *dto.CreateResponse,
	err error,
) {
	data, err = ch.create.Create(problem)
	return
}

func (ch *createHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return
}

func (ch *createHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.CreateResponse,
) {
	routeutils.ServeResponse(
		w,
		http.StatusOK,
		resp,
	)
}

// ServeHTTP implements http.Handler interface
func (ch *createHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	problemDat, err := ch.decodeBody(r.Body)

	if err != nil {
		message := "Unable to decode error: "
		ch.handleError(w, err, message)
		return
	}

	//	problemDat.UserID = ch.decodeContext(r)
	data, err := ch.askController(&problemDat)

	if err != nil {
		message := "Unable to create problem error: "
		ch.handleError(w, err, message)
		return
	}

	ch.responseSuccess(w, data)
}

// CreateParams provide parameters for CreateRoute
type CreateParams struct {
	dig.In
	Create     problem.Creater
	Middleware *middleware.Auth
}

// CreateRoute provides a route that lets to take problems
func CreateRoute(params CreateParams) *routeutils.Route {
	handler := createHandler{params.Create}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.ProblemCreate,
		Handler: params.Middleware.Middleware(&handler),
	}
}
