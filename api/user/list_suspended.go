package user

import (
	"fmt"
	"net/http"
	"strconv"

	"citi-backend/api/middleware"
	"citi-backend/api/routeutils"
	"citi-backend/apipattern"
	"citi-backend/errors"
	"citi-backend/user"
	"citi-backend/user/dto"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
)

// searchHandler holds the handler that searches for user
type listSuspendedHandler struct {
	searcher user.ListSuspender
}

// querySkip skips number of users for a request
func (list *listSuspendedHandler) querySkip(
	r *http.Request,
) (skip int, err error) {
	skip, err = strconv.Atoi(chi.URLParam(r, "skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid skip", false,
				},
			},
		)
		return
	}
	return
}

// queryLimit limits number of users per query
func (list *listSuspendedHandler) queryLimit(r *http.Request) (
	limit int, err error,
) {
	limit, err = strconv.Atoi(chi.URLParam(r, "limit"))

	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid limit", false,
				},
			},
		)
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too big", false,
			},
		}
		return
	}

	return
}

func (list *listSuspendedHandler) askController(
	skip int64,
	limit int64,
) (
	resp []dto.ReadResp,
	err error,
) {
	resp, err = list.searcher.ListSuspend(skip, limit)
	return
}

func (list *listSuspendedHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (list *listSuspendedHandler) responseSuccess(
	w http.ResponseWriter,
	resp []dto.ReadResp,
) {
	// Serve a response to the client
	routeutils.ServeResponse(
		w,
		http.StatusOK,
		resp,
	)
}

func (list *listSuspendedHandler) handleRead(
	w http.ResponseWriter,
	r *http.Request,
) {

	skip, err := list.querySkip(r)
	if err != nil {
		//message := "Unable to extract skip: "
		list.handleError(w, err)
		return
	}

	limit, err := list.queryLimit(r)
	if err != nil {
		//		message := "Unable to extract limit: "
		list.handleError(w, err)
		return
	}
	// Read request from database using request id and user id
	resp, err := list.askController(int64(skip), int64(limit))

	if err != nil {
		list.handleError(w, err)
		return
	}

	list.responseSuccess(w, resp)
}

// ServeHTTP implements http.Handler
func (list *listSuspendedHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	list.handleRead(w, r)
}

// SearchRoute provides a route that searches for users
func ListSuspendedRoute(
	searcher user.ListSuspender,
	middleware *middleware.Auth,
) *routeutils.Route {
	handler := listSuspendedHandler{searcher}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.UserListSuspended,
		Handler: middleware.Middleware(&handler),
	}
}
