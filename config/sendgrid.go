package config

import "github.com/spf13/viper"

type SendGrid struct {
	SID       string
	AuthToken string
	Phone     string
}

func LoadSendGrid() SendGrid {
	return SendGrid{
		SID:       viper.GetString("twilio.SID"),
		AuthToken: viper.GetString("twilio.AuthToken"),
		Phone:     viper.GetString("twilio.Phone"),
	}
}
