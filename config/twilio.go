package config

import "github.com/spf13/viper"

type Twilio struct {
	SID       string
	AuthToken string
	Phone     string
}

func LoadTwilio() Twilio {
	return Twilio{
		SID:       viper.GetString("twilio.SID"),
		AuthToken: viper.GetString("twilio.AuthToken"),
		Phone:     viper.GetString("twilio.Phone"),
	}
}
