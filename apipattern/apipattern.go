package apipattern

// AuthorCreate holds the api string for creating a author
const AuthorCreate string = "/api/v1/author/create"

// AuthorRead holds the api string for reading authors
const AuthorRead string = "/api/v1/author/get/{id}"

// AuthorUpdate holds the api string for updating author
const AuthorUpdate string = "/api/v1/author/update"

// AuthorDelete holds the api string for getting a author
const AuthorDelete string = "/api/v1/author/delete"

// UserList holds the api for giving user list
const AuthorList string = "/api/v1/allauthor/{skip}/{limit}"

// AuthorCreate holds the api string for creating a author
const ProblemCreate string = "/api/v1/problem/create"

// AuthorRead holds the api string for reading authors
const ProblemRead string = "/api/v1/problem/get/{id}"

// AuthorUpdate holds the api string for updating author
const ProblemUpdate string = "/api/v1/problem/update"

// AuthorDelete holds the api string for getting a author
const ProblemDelete string = "/api/v1/problem/delete"

// AuthorCreate holds the api string for creating a author
const ContestCreate string = "/api/v1/contest/create"

// AuthorRead holds the api string for reading authors
const ContestRead string = "/api/v1/contest/get/{id}"

// AuthorUpdate holds the api string for updating author
const ContestUpdate string = "/api/v1/contest/update"

// AuthorDelete holds the api string for getting a author
const ContestDelete string = "/api/v1/contest/delete"

// StaticContentCreate holds the api string for creating a staticcontent
const StaticContentCreate string = "/api/v1/staticcontent/create"

// StaticContentRead holds the api string for reading staticcontents
const StaticContentRead string = "/api/v1/staticcontent/get/{id}"

// StaticContentUpdate holds the api string for updating staticcontent
const StaticContentUpdate string = "/api/v1/staticcontent/update"

// StaticContentDelete holds the api string for getting a staticcontent
const StaticContentDelete string = "/api/v1/staticcontent/delete"

// PublicUserList holds the api for giving public user list
const PublicUserList string = "/api/v1/public/alluser"

// UserList holds the api for giving user list
const UserList string = "/api/v1/alluser"

// LoginUser holds the api for logging user
const LoginUser string = "/api/v1/users/login"

// RegistrationToken holds holds the api for giving registration token
const RegistrationToken string = "/api/v1/registration/token"

// UserRegistration holds the api for registering a user
const UserRegistration string = "/api/v1/users/registration"

// UserSearch holds the api for searching user
const UserSearch string = "/api/v1/users/search"

// UserSearch holds the api for searching user
const UserListSuspended string = "/api/v1/users/suspended/{skip}/{limit}"

// OrderCreate holds the api string for creating a author
const OrderCreate string = "/api/v1/order/create"

// OrderRead holds the api string for reading authors
const OrderRead string = "/api/v1/order/get/{id}"

// OrderUpdate holds the api string for updating order
const OrderUpdate string = "/api/v1/order/update"

// OrderDelete holds the api string for getting a order
const OrderDelete string = "/api/v1/order/delete"

// OrderDelete holds the api string for getting a order
const OrderList string = "/api/v1/order/list/{state}/{skip}/{limit}"

// OrderByRestaurant holds the api string for getting an order
const OrderByRestaurant string = "/api/v1/order/byrestaurant"

// DeliveryCreate holds the api string for creating a delivery
const DeliveryCreate string = "/api/v1/delivery/create"

// DeliveryRead holds the api string for reading a delivery
const DeliveryRead string = "/api/v1/delivery/get/{id}"

// DeliveryUpdate holds the api string for updating a delivery
const DeliveryUpdate string = "/api/v1/delivery/update"

// DeliveryDelete holds the api string for getting a delivery
const DeliveryDelete string = "/api/v1/delivery/delete"

const DeliveryByUser string = "/api/v1/delivery/byuser"

const DeliveryByDriver string = "/api/v1/delivery/bydriver/{id}"

// RestaurantCreate holds the api string for creating a restaurant
const RestaurantCreate string = "/api/v1/restaurant/create"

// RestaurantRead holds the api string for reading restaurants
const RestaurantRead string = "/api/v1/restaurant/get/{id}"

// RestaurantUpdate holds the api string for updating restaurant
const RestaurantUpdate string = "/api/v1/restaurant/update"

// RestaurantDelete holds the api string for getting a restaurant
const RestaurantDelete string = "/api/v1/restaurant/delete"

// UserSearch holds the api for searching user
const RestaurantListSuspended string = "/api/v1/restaurant/suspended"

// UserSearch holds the api for searching user
const RestaurantList string = "/api/v1/restaurant/list"

// SMSSend holds the api string for sending sms
const SMSSend string = "/api/v1/notif/send"

// EmailSend holds the api string for sending email
const EmailSend string = "/api/v1/notif/sendemail"

// MenuItemCreate holds the api string for creating a restaurant
const MenuItemCreate string = "/api/v1/menuitem/create"

// MenuItemRead holds the api string for reading restaurants
const MenuItemRead string = "/api/v1/menuitem/get/{id}"

// MenuItemUpdate holds the api string for updating restaurant
const MenuItemUpdate string = "/api/v1/menuitem/update"

// MenuItemDelete holds the api string for getting a restaurant
const MenuItemDelete string = "/api/v1/menuitem/delete"

// StateCreate holds the api string for creating a state
const StateCreate string = "/api/v1/state/create"

// StateRead holds the api string for reading state
const StateRead string = "/api/v1/state/get/{id}"

// StateUpdate holds the api string for updating state
const StateUpdate string = "/api/v1/state/update"

// StateDelete holds the api string for getting a state
const StateDelete string = "/api/v1/state/delete"

// MapRouteGet holds the api string for getting a state
const MapRouteGet string = "/api/v1/maproute/get"

// UserSearch holds the api for searching user
const SpecificUserList string = "/api/v1/user/list"
