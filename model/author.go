package model

import "time"

// Author defines Author model
type Author struct {
	ID        string
	Name      string
	Intro     string
	Tagline   string
	IsDeleted bool
	CreatedAt time.Time
	UpdatedAt time.Time
	Contests  []string
	Problems  []string
}

type Credentials struct {
	Name      string
	Phone     string
	Address   string
	Latitude  float64
	Longitude float64
}
