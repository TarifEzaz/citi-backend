package model

import "time"

// Contest defines Contest model
type Contest struct {
	ID         string
	Name       string
	Date       string
	LandingURL string
	ImageURL   string
	Stadings   string
	IsDeleted  bool
	Note       string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}
