package model

import "time"

// Problem defines Problem model
type Problem struct {
	ID         string
	Title      string
	Slug       string
	Link       string
	Note       string
	CreatedAt  time.Time
	UpdatedAt  time.Time
	Name       string
	Category   []string
	Authors    []string
	Contest    string
	Difficulty float32
}
